# -*- coding: utf-8 -*-
"""
Exception classes that are unique to this package.
"""


class NoAssociatedAPIError(RuntimeError):
    """
    Raised when the programmer has instantiated a Screenshot Monitor object without using the API.
    This is fine until they try to perform an action on it like delete it.
    """
    pass

class APIError(Exception):
    """
    Raised whenever the Screenshot Monitor API returned an error of some kind.
    """
    pass
