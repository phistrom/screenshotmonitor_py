# screenshotmonitor-py

An unofficial Python library for accessing [ScreenshotMonitor.com]'s RESTful v2 API.

  - Search for employees
  - See activities done in a date range
  - Restore, archive, and delete employees.

## Requires
  - [requests] *Python HTTP Requests for Humans by Kenneth Reitz*
  - [six] *Python 2 and 3 Compatibility Library*

## Installation

```sh
pip install screenshotmonitor-py
```

License
----
Apache

   [ScreenshotMonitor.com]: <https://screenshotmonitor.com/>
   [requests]: <https://github.com/kennethreitz/requests>
   [six]: <http://pythonhosted.org/six/>
