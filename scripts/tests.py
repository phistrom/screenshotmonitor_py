# -*- coding: utf-8 -*-
"""

"""

from datetime import datetime, timedelta
import sys
import time

from screenshotmonitor import connect


def last_2_weeks_of_activities_all_employees(api):
    for employee in api.employees:
        print("%s:" % employee.name)
        for activity in employee.get_activities(start=datetime.now() - timedelta(weeks=16)):
            print("\t%s" % activity)
        time.sleep(1)
        print("\n\n")


def main(key):
    api = connect(key)

    # for employee in api.employees:
    #     if employee.last_active is not None and not employee.archived:
    #         print("%s %s" % (employee.name, employee.last_active < datetime.now() - timedelta(weeks=2)))

    for employee in api.employees:
        print("%s" % employee)

    #last_2_weeks_of_activities_all_employees(api)

    # for employee in api.search_employees('hard workin guy'):
    #     print(employee)
    #     employee.delete()


if __name__ == "__main__":
    main(sys.argv[1])
